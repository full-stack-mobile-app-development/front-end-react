# Troubleshooting

## Question

> Find the version of an installed npm package

Answer

> `npm list` for local packages or `npm list -g` for globally installed packages or `npm list grunt` for specific package

## Question

> Install a previous version of a package

## Answer

`$ npm install <package>@<version>`

For example: `npm install express@3.0.0`

## Trouble

Running create-react-app raises *info No lockfile found.* and stops creating the app

## Solution

Just remove the file .npmrc

`$ rm c:\Users\filipebezerra\.npmrc`